﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PROTECTED_StudentLayer_MasterStudent : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Logiczna.Student)
        {
            Response.Redirect("~/Logowanie.aspx");
        }
    }
}
