﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PROTECTED/AdminLayer/MasterAdmin.master" AutoEventWireup="true" CodeFile="DodajPrzedmiot.aspx.cs" Inherits="PROTECTED_AdminLayer_DodajPrzedmiot" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div><h1>Dodaj Przedmiot</h1></div>
    <div>
        Nazwa Przedmiotu:<asp:TextBox ID="TextBox_nazwa" runat="server"></asp:TextBox><br />
        Liczba godzin wykładu:<asp:TextBox ID="TextBox_wyk_l_godzin" runat="server"></asp:TextBox><br />
        Liczba godzin laboratorium:<asp:TextBox ID="TextBox_lab_l_godzin" runat="server"></asp:TextBox><br />
        Egzamin:<asp:CheckBox ID="CheckBox_egzamin" runat="server" /><br />
        Rodzaj Przedmiotu:<asp:TextBox ID="TextBox_rodzaj" runat="server"></asp:TextBox><br />
        <asp:Button ID="Button1" runat="server" Text="Dodaj Przedmiot" OnClick="Button1_Click" />
        <asp:Button ID="Button2" runat="server" Text="Pokaż Przedmioty" OnClick="Button2_Click" />
        
    </div>
</asp:Content>

