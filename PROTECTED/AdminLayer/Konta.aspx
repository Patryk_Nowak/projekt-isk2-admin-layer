﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PROTECTED/AdminLayer/MasterAdmin.master" AutoEventWireup="true" CodeFile="Konta.aspx.cs" Inherits="PROTECTED_AdminLayer_Konta" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
 
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h1>Konta</h1>
    <div>
        <label>Konta studentów</label>
            <br />
        <br />
            <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
            <asp:GridView ID="GridView1" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None" AutoGenerateColumns="False" DataKeyNames="Id" OnRowDeleting="GridView1_RowDeleting" OnRowCancelingEdit="GridView1_RowCancelingEdit" OnRowEditing="GridView1_RowEditing" OnRowUpdating="GridView1_RowUpdating" EnableModelValidation="True" ShowFooter="True">
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                <Columns>
                    <asp:CommandField ShowEditButton="True" />
                    <asp:BoundField DataField="Id" HeaderText="id" Visible="False" />
                    <asp:BoundField DataField="nr_indeksu" HeaderText="Numer indeksu" />
                    <asp:BoundField DataField="imie" HeaderText="Imię" />
                    <asp:BoundField DataField="nazwisko" HeaderText="Nazwisko" />
                    <asp:BoundField DataField="adres" HeaderText="Adres" />
                    <asp:BoundField DataField="data_urodzenia" HeaderText="Data urodzenia" />
                    <asp:BoundField DataField="pesel" HeaderText="Pesel" />
                    <asp:BoundField DataField="data_rozpoczecia" HeaderText="Data rozpoczęcia studiów" />
                    <asp:BoundField DataField="aktualny_sem" HeaderText="Aktualny semestr" />
                    <asp:BoundField DataField="tryb_studiow" HeaderText="Tryb studiów" />
                    <asp:BoundField DataField="idkierunku" HeaderText="Kierunek" />
                    <asp:BoundField DataField="mail" HeaderText="Adres e-mail" />
                    <asp:BoundField DataField="haslo" HeaderText="Hasło" />
                    <asp:CommandField ShowDeleteButton="True" />
                </Columns>
                <EditRowStyle BackColor="#999999" />
                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            </asp:GridView>
    </div>
    <br />
    <br />
     <div>
        <label>Konta wykładowców</label>
             <br />
         <br />
            <asp:GridView ID="GridView2" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None" AutoGenerateColumns="False"  DataKeyNames="Id" OnRowDeleting="GridView2_RowDeleting" OnRowCancelingEdit="GridView2_RowCancelingEdit" OnRowEditing="GridView2_RowEditing" OnRowUpdating="GridView2_RowUpdating" EnableModelValidation="True" ShowFooter="True">
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                <Columns>
                    <asp:CommandField ShowEditButton="True" />
                    <asp:BoundField DataField="Id" HeaderText="Id" Visible="False" />
                    <asp:BoundField DataField="imie" HeaderText="Imię" />
                    <asp:BoundField DataField="nazwisko" HeaderText="Nazwisko" />
                    <asp:BoundField DataField="katedra" HeaderText="Katedra" />
                    <asp:BoundField DataField="email" HeaderText="Adres e-mail" />
                    <asp:BoundField DataField="login" HeaderText="Login" />
                    <asp:BoundField DataField="haslo" HeaderText="Hasło" />
                    <asp:CommandField ShowDeleteButton="True" />
                </Columns>
                <EditRowStyle BackColor="#999999" />
                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            </asp:GridView>
    </div>
</asp:Content>

